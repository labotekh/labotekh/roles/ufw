# ufw

[![Maintainer](https://img.shields.io/badge/maintained%20by-labotekh-e00000?style=flat-square)](https://gilab.com/labotekh)
[![License](https://img.shields.io/badge/License-MIT-yellow.svg)](https://gitlab.com/labotekh/iac/roles/ufw/-/blob/main/LICENSE)
[![Release](https://gitlab.com/labotekh/iac/roles/ufw/-/badges/release.svg)](https://gitlab.com/labotekh/iac/roles/ufw/-/releases)[![Ansible version](https://img.shields.io/badge/ansible-%3E%3D2.17-black.svg?style=flat-square&logo=ansible)](https://github.com/ansible/ansible)

Open the specified ports and protocols additionnaly to the custom SSH port

**Platforms Supported**:

| Platform | Versions |
|----------|----------|
| Ubuntu | 24.04.1 |

## ⚠️ Requirements

Ansible >= 2.17.

### Ansible role dependencies

  * {'role': 'ssh'}
## ⚡ Installation

### Install with git

If you do not want a global installation, clone it into your `roles_path`.

```bash
git clone https://gitlab.com/labotekh/iac/roles/ufw.git  ufw
```

But I often add it as a submodule in a given `playbook_dir` repository.

```bash
git submodule add https://gitlab.com/labotekh/iac/roles/ufw.git roles/ufw
```

### ✏️ Example Playbook

Basic usage is:

```yaml
- hosts: all
  roles:
    - role: ufw
      vars:
        required_ports:
          - port: XXXXX
            proto: any | tcp | udp | ipv6 | esp | ah | gre | igmp
            rule: allow | deny | limit | reject
        
```

## ⚙️ Role Variables

Variables are divided in three types.

The **default vars** section shows you which variables you may
override in your ansible inventory. As a matter of fact, all variables should
be defined there for explicitness, ease of documentation as well as overall
role manageability.

The **context variables** are shown in section below hint you
on how runtime context may affects role execution.

### Default variables

#### main

Open the specified ports and protocols additionnaly to the custom SSH port

| Variable Name | Required | Type | Default | Elements | Description |
|---------------|----------|------|---------|----------|-------------|
| required_ports | True | list |  | dict | The list of ports to open |

## Author Information

Imotekh <imotekh@protonmail.com>